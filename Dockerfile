FROM python:3.6

WORKDIR /app

ADD app .

RUN pip install -r requeriments.txt && apt update && apt install sqlite3

EXPOSE 5000

CMD ["python","main.py"]
