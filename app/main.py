from flask import Flask, jsonify, request
from config import DevConfig

app = Flask(__name__)
app.config.from_object(DevConfig)

stores = [
    {
        'name': 'Mi maravillosa tienda',
        'items': [
            {
                'name': 'Mi producto',
                'price': 15.99,
            }
        ]
    }
]

@app.route('/')
def home():
    return '<h1>Hola mundo</h1>'

@app.route('/store', methods=['GET'])
def get_all_stores():
    return jsonify(stores)

@app.route('/store', methods=['POST'])
def add_store():
    request_data = request.get_json()
    new_store = {
        'name': request_data['name'],
        'items': []
    }    
    stores.append(new_store)
    return jsonify(new_store)

@app.route('/store/<store>', methods=['GET'])
def get_store(store):
    for i in stores:
        if i['name'] == store:
            return jsonify(i)
    return '{"message": "No existe la tienda elegida"}'

@app.route('/store/<store>/item', methods=['GET'])
def get_store_items(store):
    for i in stores:
        if i['name'] == store:
            return jsonify(i['items'])
    return '{"message": "No existe la tienda elegida"}'

@app.route('/store/<store>/item', methods=['POST'])
def add_store_item(store):
    pass

@app.route('/store/<store>/item/<item>', methods=['GET'])
def get_store_item(store,item):
    for i in stores:
        if i['name'] == store:
            for j in i['items']:
                if j['name'] == item:
                    return jsonify(j)
    return '{"message": "No existe la tienda elegida"}'

if __name__ == '__main__':
    app.run(host = '0.0.0.0')
