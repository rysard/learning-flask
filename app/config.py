class Config(object):
    pass

class ProdConfig(Config):
    pass

class DevConfig(Config):
    Debug = True
    # This variable allows the connection with the database
    SQLALCHEMY_DATABASE_URI = 'sqlite:///prueba.db'
    # To see how SQLAlchemy create the queries uncomment next line
    # SQLALCHEMY_ECHO = True